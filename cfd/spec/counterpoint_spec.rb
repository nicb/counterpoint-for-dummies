require 'spec_helper'

describe Cfd::Counterpoint do
  
  before :example do
    @cantus_firmus= Cfd::Voice.new([
      Cfd::Note.new(0, 1, 0, 7),
      Cfd::Note.new(1, 1, 4, 7),
      Cfd::Note.new(2, 1, 2, 7),
      Cfd::Note.new(3, 1, 7, 7),
      Cfd::Note.new(4, 1, 9, 7),
      Cfd::Note.new(5, 1, 7, 7),
      Cfd::Note.new(6, 1, 4, 7),
      Cfd::Note.new(7, 1, 5, 7),
      Cfd::Note.new(8, 1, 2, 7),
      Cfd::Note.new(9, 1, 0, 7),
    ])
  end

  it 'writes the last note' do
    expect((cf=Cfd::Counterpoint.new(@cantus_firmus)).class).to be(Cfd::Counterpoint)
    cf.fill
    expect(cf.voices.last.last.pc).to eq(0)
    expect(cf.voices.last.last.oct==7 || cf.voices.last.last.oct==8).to be(true)
  end

end
