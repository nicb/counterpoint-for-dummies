module Cfd

  class Rule

    def apply(cf, idx, ov, n=nil) #other voice
      raise StandardError, "Pure virtual method called"
    end
    
  end

  class GenerativeRule<Rule

    def apply(cf, idx, ov, n=nil)
      raise StandardError, "Pure virtual method called"
    end
    
  end

  class FilterRule<Rule

    def apply(cf, idx, ov, n)
      raise StandardError, "Pure virtual method called"
    end
    
  end

end
