module Cfd

  class Note
    attr_accessor :pc, :oct, :dur, :at, :instr, :dyn #i due punti indicano che quella  variabile è un simbolo,rappresenta un valore testuale
    
    URDO = 1.021875
    NAME_MAP = %w(do do# re re# mi fa fa# sol sol# la la# si)
    DYN_MAP = { 'fff'=>-6,'ff'=>-10,'f'=>-14,'mf'=>-18,'mp'=>-22,'p'=>-26,'pp'=>-32,'ppp'=>-36, }

    def name
      NAME_MAP[self.pc]+(self.oct-5).to_s
    end
    
    def freq
      oct_freq=URDO*(2.0**@oct)
      oct_freq*(2.0**(@pc/12.0))
    end
  
    def initialize(at=0.0,dur=0.5,pc=0,oct=8,dyn='mf',instr=1)
      @pc=pc
      @oct=oct
      @at=at
      @dur=dur  
      @dyn=dyn
      @instr=instr
    end
  
    def to_csound
      "i%d %8.4f %8.4f %+8.3f %8.4f" % [self.instr, self.at, self.dur,DYN_MAP[self.dyn], self.freq]
    end
  
  end

end
