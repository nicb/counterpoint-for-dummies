module Cfd

  class Counterpoint 
    attr_reader :ruleset, :voices
  
    def initialize(cf)
      @ruleset = RuleSet.new
      @voices = []
      @voices << cf
    end
    
    def cantus_firmus
      self.voices.first
    end

    def fill
      other_voice = Voice.new
      rev = cantus_firmus.reverse
      idx = cantus_firmus.size-1
      rev.each do
        |n|
        other_voice << apply_ruleset(idx, other_voice)
        idx-=1
      end
      @voices << other_voice.reverse
    end
  
  private
  
    def apply_ruleset(idx, ov)
      self.ruleset.apply(cantus_firmus, idx, ov)
    end	
  
  end

end
