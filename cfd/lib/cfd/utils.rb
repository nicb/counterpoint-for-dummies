module Cfd
  module Utils
    PATH=File.expand_path(File.join('..', 'utils'), __FILE__)
  end
end

%w(
  factory
).each { |f| require File.join(Cfd::Utils::PATH, f)}
