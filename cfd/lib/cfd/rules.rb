module Cfd

  class LastNoteRule < GenerativeRule

    def apply(cf, idx, ov, n=nil)
      ncf=cf[idx]
      intv=rand()< 0.5 ? 0 : 12
      pc=((ncf.pc+intv)%12).to_i
      oct=intv==12 ? ncf.oct+1 : ncf.oct
      Note.new(ncf.at, ncf.dur, pc, oct)
    end

  end

end
