module Cfd
  module Utils

  	class Factory < Array

  	  def get
  	  	res = nil
  	  	idx = (rand()*(self.size-1)).round
  	  	res = self[idx]
  	  	self.delete_at(idx)
  	  	res
  	  end

  	end

  end
end