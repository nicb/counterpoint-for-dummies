module Cfd

  class RuleSet < Hash

    def apply(cf,idx, ov)
      key =case idx
      when 0 then :first
      when cf.size-1 then :last
      else :middle
      end
      note=mynote=nil
      while true 
        break unless self.has_key?(key) && !self[key].empty?
        note = mynote = self[key][0].apply(cf, idx, ov, mynote)
        self[key][1..-1].each do
          |rule|
          note = rule.apply(cf, idx, ov, note)
          break unless note
        end
        break if note || !mynote
      end
      note
    end
    
    def add_rule(key,r)
      self.update(key => []) unless self.has_key?(key)
      self[key] << r
    end
      
    def initialize
      load_rules
    end
    
  private
  
    def load_rules
      add_rule(:last, LastNoteRule.new)
    end 
  
  end

end
